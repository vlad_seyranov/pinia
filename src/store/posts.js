import { defineStore } from "pinia";

export const usePostsStore = defineStore('posts', {
  state: () => ({
    todos: [],
  }),
  actions: {
    async fetchTodo() {
      try {
        const res = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=1')
        const data = await res.json()
        this.todos = data
      } catch (error) {console.error('Error fetching data:', error)}
    },
    createTodo(newTodo) {
      this.todos.unshift(newTodo)
    },
    deleteTodo(todoItem) {
      this.todos = this.todos.filter(item => item.id !== todoItem.id)
    },
  },
  getters: {
    getTodo(state) {
      return state.todos
    }
  }
})
