import App from './App.vue'
import components from '@/components/UI'

import { createApp } from 'vue'

import { registerPlugins } from '@/plugins'

const app = createApp(App)

//Регистрация компонентов
for (const componentName in components) {
  const component = components[componentName];
  app.component(component.name, component);
}

registerPlugins(app)

app.mount('#app')
