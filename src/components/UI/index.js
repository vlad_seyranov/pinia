import CustomSnackbar from "@/components/UI/CustomSnackbar";
import CustomTextField from "@/components/UI/CustomTextField";
import CustomToolbar from "@/components/UI/CustomToolbar";
import CustomDialog from "@/components/UI/CustomDialog";

export default {
  CustomSnackbar,
  CustomTextField,
  CustomToolbar,
  CustomDialog
}
